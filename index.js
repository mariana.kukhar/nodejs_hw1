const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const morgan = require('morgan')

app.use(express.json());
app.use(morgan('tiny'));

const currentFiles = [];
const entryDirection = path.join(__dirname, '/files');
fs.readdir(entryDirection, (err, files) => {
  files.forEach((file) => {
    if (file !== 'files.txt') {
      console.log(file);
      entryDirection.push(file);
    }
  });
});

app.get('/files/:filename', (req, res) => {
  try {
    const filename = req.params.filename;

    if (filename) {
      const result = currentFiles.find((file) => file === filename);
      if (result) {
        const content = fs.readFileSync(path.join(entryDirection, result)).toString();
        const extension = path.extname(filename);
        const uploadedDate = stats.birthtime;

        res.status(200).send({
          message: 'Success',
          filename,
          content,
          extension,
          uploadedDate
        });
      } else {
        res.status(400).send({ message: `No file with ${filename} filename found` });
      }
    }
  } catch (err) {
    res.status(500).send({ message: 'Server error' });
  }
});

app.get('/files', (req, res) => {
  try {
    if (currentFiles.length >= 0) {
      res.status(200).send({
        message: 'Success',
        files: currentFiles
      });
    } else {
      res.status(400).send({ message: 'Client error' });
    }
  } catch (err) {
    res.status(500).send({ message: 'Server error' });
  }
});

app.post('/files', (req, res) => {
  const filename = req.body.filename;
  const content = req.body.content;

  const validFile = '^.*\\.(log|txt|json|yaml|xml|js)$'

  if (!content) {
      return res.status(400).json({message: "Please specify 'content' parameter"});
  }
  if (filename.match(validFile)) {
      fs.writeFile(path.join(entryDirection, filename), content, (err) => {
          if (!err) {
              res.status(200).json({message: 'File created successfully'});
          } else {
              res.status(500).json({message: 'Server error'});
              throw err;
          }
      });
  } else {
      return res.status(400).json({message: "Please specify correct file type"});
  }
});


app.delete('/files/:filename', (req, res) => {
  const { filename } = req.params.filename;

    try {
        fs.unlink(filesPath(filename));
    } catch (err) {
        return res.status(400).json({ message: `No file with '${filename}' filename found` });
    }

    res.status(200).json({ message: 'File was successfully deleted' });
});

app.listen(8080, () => {
  console.log('Server works at port 8080!');
})